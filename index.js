// Idealmente separaría las clases en distintos archvos para organizar el código
// Pero creo que así es más fácil de compartir y corregir

class Unidad {
    constructor(tipo) {
        this.tipo = tipo;
    }

    getPuntosFuerza() {
        return this.tipo.puntosFuerza;
    }

    addPuntosFuerza(puntos) {
        this.tipo.puntosFuerza += puntos;
    }

    entrenar(monedas) {
        if(isNaN(monedas)){
            console.log("Las monedas deben ser numericas");
            return;
        }
        if(monedas >= this.tipo.costoEntrenamiento) {
            this.addPuntosFuerza(this.tipo.beneficioEntrenamiento);
            return monedas - this.tipo.costoEntrenamiento;
        }else{
            console.log("No alcanzan las monedas para entrenar");
            return monedas;
        }
    }

    transformar(monedas) {
        if(isNaN(monedas)){
            console.log("Las monedas deben ser numericas");
            return;
        }
        if(!this.tipo.costoTransformacion){
            console.log("No se puede transformar");
            return monedas;
        }

        if(monedas >= this.tipo.costoTransformacion){
            this.tipo = this.tipo.transformar();
            return monedas - this.tipo.costoTransformacion;
        }else{
            console.log("No alcanzan las monedas para transformar");
        }
    }
}

class Piquero {
    constructor() {
        this.puntosFuerza = 5;
        this.beneficioEntrenamiento = 3;
        this.costoEntrenamiento = 10;
        this.costoTransformacion = 30;
    }

    transformar() {
        return new Arquero();
    }
}

class Arquero {
    constructor() {
        this.puntosFuerza = 10;
        this.beneficioEntrenamiento = 7;
        this.costoEntrenamiento = 20;
        this.costoTransformacion = 40;
    }

    transformar() {
        return new Caballero();
    }
}

class Caballero {
    constructor() {
        this.puntosFuerza = 20;
        this.beneficioEntrenamiento = 10;
        this.costoEntrenamiento = 30;
    }
}

class Civilizacion {
    constructor(piqueros, arqueros, caballeros){
        this.civilizacion = [];
        for (let i = 0; i < piqueros; i++) {
            this.civilizacion.push(new Unidad(new Piquero()));
        }

        for (let i = 0; i < arqueros; i++) {
            this.civilizacion.push(new Unidad(new Arquero()));
        }

        for (let i = 0; i < caballeros; i++) {
            this.civilizacion.push(new Unidad(new Caballero()));
        }
    }

    getCivilizacion() {
        return this.civilizacion;
    }
}

class Chinos extends Civilizacion {
    constructor(){
        super(2, 25, 2)
    }
}

class Ingleses extends Civilizacion {
    constructor(){
        super(10, 10, 10)
    }
}

class Bizantinos extends Civilizacion {
    constructor(){
        super(5, 8, 15)
    }
}

class Ejercito {
    constructor(civilizacion) {
        this.civilizacion = civilizacion.getCivilizacion();
        this.historial = [];
        this.monedas = 1000;
    }

    getPuntos() {
        let puntos = 0;
        this.civilizacion.forEach(civil => {
            puntos += civil.getPuntosFuerza();
        });
        return puntos;
    }

    entrenar(i) {
        // Agrego este metodo para poder hacer pruebas
        if(i > this.civilizacion.length - 1) {
            console.log("La unidad no esta dentro del ejercito")
        }else {
            this.monedas = this.civilizacion[i].entrenar(this.monedas);
        }
    }

    transformar(i) {
        // Agrego este metodo para poder hacer pruebas
        if(i > this.civilizacion.length - 1) {
            console.log("La unidad no esta dentro del ejercito")
        }else {
            this.monedas = this.civilizacion[i].transformar(this.monedas);
        }
    }

    ganarBatalla() {
        this.monedas += 1000;
    }

    perderBatalla() {
        this.civilizacion.sort((civil, otroCivil) => (civil.getPuntosFuerza() - otroCivil.getPuntosFuerza()));
        this.civilizacion.pop();
        this.civilizacion.pop();
    }

    empatarBatalla() {
        this.civilizacion.pop();
    }

    agregarAlHistorial(ejercitoBatalla){
        this.historial.push(ejercitoBatalla);
    }

    atacar(otroEjercito) {
        this.agregarAlHistorial(otroEjercito);
        otroEjercito.agregarAlHistorial(this);

        const puntosEjercito = this.getPuntos();
        const puntosOtroEjercito = otroEjercito.getPuntos();
        
        if(puntosEjercito > puntosOtroEjercito){
            this.ganarBatalla();
            otroEjercito.perderBatalla();
        }else if (puntosEjercito < puntosOtroEjercito){
            otroEjercito.ganarBatalla();
            this.perderBatalla();
        }else {
            this.empatarBatalla();
            otroEjercito.empatarBatalla();
        }
    }
}

// Pruebas
// ejChino = new Ejercito(new Chinos());
// ejIngles = new Ejercito(new Ingleses());
// ejChino.entrenar(0);
// ejChino.entrenar(0);
// ejChino.entrenar(0);
// ejChino.entrenar(0);
// ejChino.entrenar(29);
// ejChino.transformar(28);
// ejChino.transformar(1);
// ejChino.atacar(ejIngles);
// console.log(ejChino)
